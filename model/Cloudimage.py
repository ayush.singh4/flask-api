from sqlalchemy import Column, Integer, String, LargeBinary
from sqlalchemy.ext.declarative import declarative_base

Base1 = declarative_base()

class Cloudimage(Base1):
    __tablename__ = 'cloudimages'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    image_data = Column(String(255))


    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'image_data': self.image_data
        }

    
