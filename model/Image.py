from sqlalchemy import Column, Integer, String, LargeBinary
from sqlalchemy.ext.declarative import declarative_base
import base64

Base = declarative_base()

class Image(Base):
    __tablename__ = 'images'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    image_data = Column(LargeBinary)


    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'image_data': base64.b64encode(self.image_data).decode()
        }

    
