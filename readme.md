# Flask API

> This is a simple API built using the Flask web framework in Python. It allows users to upload, update, delete and get images using Cloudinary.

# Dependencies

> Flask
> Installation
> To install the dependencies, use the following command:

# Upload an image

To upload an image, you need to make a POST request to the endpoint /upload with the image file in the multipart/form-data format. The response will include the image's public ID and URL.

# Update an image

To update an image, you need to make a PUT request to the endpoint /put/<int:id> with the updated image file in the multipart/form-data format. The response will include the updated image's public ID and URL.

# Delete an image

To delete an image, you need to make a DELETE request to the endpoint /delete/<int:id>. The response will include a message indicating that the image has been deleted.

# Get an image

To get an image, you need to make a GET request to the endpoint / . The response will include the image's URL.

# Error Handling

The API will return appropriate error messages for invalid requests or requests that result in errors.
