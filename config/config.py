import os
from pathlib import Path
from dotenv import load_dotenv
from sqlalchemy import create_engine
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

ENVIRONMENT = 'dev'
DB_CREDENTIALS = {
    'username': os.getenv('DB_USERNAME') or '{username}',
    'password': os.getenv('DB_PASSWORD') or '{password}',
    'host': os.getenv('DB_HOST') or '{host}',
    'port': os.getenv('DB_PORT') or 3306,
    'database': os.getenv('DB_NAME') or '{database}'
}

DATABASE_URI = ''
if ENVIRONMENT == 'dev':
    DATABASE_URI = \
        'mysql+pymysql://{username}:{password}@{host}:{port}/{database}'.\
        format(
            username=DB_CREDENTIALS.get('username'),
            password=DB_CREDENTIALS.get('password'),
            host=DB_CREDENTIALS.get('host'),
            port=DB_CREDENTIALS.get('port'),
            database=DB_CREDENTIALS.get('database')
        )


engine = create_engine('mysql+pymysql://root:password@localhost:3306/images')