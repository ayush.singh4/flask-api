from flask import Flask
from routes.ImageRoute import bp
from controller import *
import os

app = Flask(__name__)
app.config.from_object('config')
app.register_blueprint(bp, url_prefix='/')
app.config['UPLOAD_FOLDER'] = 'upload'

if __name__ == '__main__':
    app.debug = True
    app.run()
