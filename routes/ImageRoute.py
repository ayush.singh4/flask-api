from flask import Blueprint

from controller.ImageController import upload_image, update_image, delete_image,get_images,upload_imageurl

bp = Blueprint('bp', __name__)

bp.route('/', methods=['GET'])(get_images)
bp.route('/upload', methods=['POST'])(upload_image)
bp.route('/put/<int:id>', methods=['PUT'])(update_image)
bp.route('/delete/<int:id>', methods=['DELETE'])(delete_image)
bp.route('/uploadurl',methods=['POST'])(upload_imageurl)
