from flask import Flask, request, jsonify
import cloudinary
import cloudinary.uploader
from werkzeug.utils import secure_filename
from flask_mysqldb import MySQL
from model.database import session
from model.database import session1
from model.Cloudimage import Cloudimage 
from model.Image import Image
from cloudinary.uploader import upload
from cloudinary.utils import cloudinary_url
import os

app = Flask(__name__)
mysql = MySQL(app)
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['UPLOAD_FOLDER'] = 'upload'

cloudinary.config(
  cloud_name = "dcfzo1ylo",
  api_key = "467537558217291",
  api_secret = "wBUAQd3v8sw7q2I39ndyXw5EH6Q"
)

def upload_image():
    print(request.files)

    if 'image' not in request.files:
        return jsonify({'error': 'No image provided'}), 400
    file = request.files['image']
    if file and allowed_file(file.filename):
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
        image_data = file.read()
        new_image = Image(name=file.filename, image_data=image_data)
        session.add(new_image)
        session.commit()
        return jsonify({'message': 'Image uploaded successfully'}), 201
    else:
        return jsonify({'message': 'Invalid file type'}), 400

def update_image(id):
    file = request.files['image']
    if file and allowed_file(file.filename):
        image = session.query(Image).filter_by(id=id).first()
        if image:
            if os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'], image.name)):
                os.remove(os.path.join(app.config['UPLOAD_FOLDER'], image.name))
        
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))           
            image_data = file.read()
            image.image_data = image_data
            image.name = file.filename
            session.commit()
            return jsonify({'message': 'Image updated successfully'}), 200
        else:
            return jsonify({'message': 'Image not found'}), 404
    else:
        return jsonify({'message': 'Invalid file type'}), 400



def delete_image(id):
    image = session.query(Image).filter_by(id=id).first()
    if image:
        if os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'], image.name)):
            os.remove(os.path.join(app.config['UPLOAD_FOLDER'], image.name))
      
        session.delete(image)
        session.commit()
        return jsonify({'message': 'Image deleted successfully'}), 200
    else:
        return jsonify({'message': 'Image not found'}), 404


def get_images():
    images = session.query(Image).all()
    if images:
        images_list = [image.to_dict() for image in images]
        return jsonify({'images': images_list}), 200
    else:
        return jsonify({'message': 'No images found'}), 404

def upload_imageurl():
    if 'image' not in request.files:
        return jsonify({'error': 'No image provided'}), 400
    file = request.files['image']
    print(file)
    if file and allowed_file(file.filename):
        # file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
        uploaded_file = cloudinary.uploader.upload(file)
        image_url = uploaded_file['url']
        new_image = Cloudimage(name=file.filename, image_data=image_url)
        session1.add(new_image)
        session1.commit()
        return jsonify({'message': 'Image uploaded successfully'}), 201
    else:
        return jsonify({'message': 'Invalid file type'}), 400




def allowed_file(filename):
    ALLOWED_EXTENSIONS = {'jpg', 'jpeg', 'png', 'gif'}
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS        